pico-8 cartridge // http://www.pico-8.com
version 29
__lua__
--main
function _init()
  titleinit()
end
-->8
--title code
function titleinit()
  toggle_music(0)
  _update60=titleupdate
  _draw=titledraw
end

function titleupdate()
  if btnp(5) then
    toggle_music(0)
    gameinit()
  end
end

function titledraw()
  cls()
  draw_stars()
  spr(17,36,32,8,2)
  text="press ❎ to play"
  print(text,hcenter(text),96)
end
-->8
--game code
function gameinit()
  clrs={3,4,5,6,11,12,13}
  gndclr=clrs[flr(rnd(#clrs))+1]
  game_over=false
  g=0.020 --gravity
  make_player()
  make_ground()
  make_sparks()
  if (not lvlup) score = 0
		lvlup=false
  fuel = 500
  _update60=gameupdate
  _draw=gamedraw
end

function gameupdate()
  if (game_over) then
    if (btnp(4)) titleinit()
  elseif (lvlup) then
    if (btnp(5)) gameinit()
    if (btnp(4)) titleinit()
  else
    move_player()
    check_land()
    calculate_fuel()
  end

  for i=1,#sparks do
    if sparks[i].alive then
      sparks[i].x += sparks[i].velx / sparks[i].mass
      sparks[i].y += sparks[i].vely / sparks[i].mass
      sparks[i].r -= 0.1
      
      if sparks[i].r < 0.1 then
        sparks[i].alive = false
      end
    end
  end
end

function gamedraw()
  cls()
  draw_stars()
  draw_ground()
  draw_player()
  draw_sparks()
    print("fuel: "..fuel,0,0,7)
  
  if (lvlup) then
    gotext="level completed!"
    print(gotext,hcenter(gotext),48,11)
    scprt="score: "
    text="press ❎ to play next level"
    end_print()
  elseif(game_over) then
    if (fuel < 1) then
      gotext = "you ran out of fuel"
    else
      gotext = "you died!"
    end
    scprt="final score: "
    print(gotext,hcenter(gotext),48,8)
    end_print()
  end
end

function end_print()
  scoretxt=scprt..score
  print(scoretxt,hcenter(scoretxt),57,57,8)
  if (lvlup) print(text,hcenter(text),70,5)
  text2="press 🅾️ to return to title"
  print(text2,hcenter(text2),80,5)
end
-->8
--objects

--player creation
function make_player()
  p={}
  p.x=60 --position
  p.y=8
  p.dx=0 --movement
  p.dy=0
  p.sprite=1
  p.alive=true
  p.thrust=0.050
end

function draw_player()
  if (not game_over) then
    spr(p.sprite,p.x,p.y)
  elseif (game_over and win) then
    spr(p.sprite,p.x,p.y)
    spr(4,p.x,p.y-8) --flag
  end
end

--ground creation
function make_ground()
  --create the ground
  gnd={}
  local top=96 --highest point
  local btm=120 --lowest point
  
  --set up the landing pad
  pad={}
  pad.width=15
  pad.x=rndb(0,126-pad.width)
  pad.y=rndb(top,btm)
  pad.sprite=2
  
  --create ground at pad
  for i=pad.x,pad.x+pad.width do
    gnd[i]=pad.y
  end
  
  --create ground right of pad
  for i=pad.x+pad.width+1,127 do
    local h=rndb(gnd[i-1]-3,gnd[i-1]+3)
    gnd[i]=mid(top,h,btm)
  end

  --create ground left of pad
  for i=pad.x-1,0,-1 do
    local h=rndb(gnd[i+1]-3,gnd[i+1]+3)
    gnd[i]=mid(top,h,btm)
  end
end

function draw_ground()
  for i=0,127 do
    line(i,gnd[i],i,127,gndclr)
  end
  spr(pad.sprite,pad.x,pad.y,2,1)
end

--draw stars
function draw_stars()
  srand(1)
  for i=1,50 do
    pset(rndb(0,127),rndb(0,127),rndb(5,7))
  end
  srand(time())
end

--make sparks
function make_sparks()
  sparks={}
  
  for i=1,200 do
    add(sparks,{
      x=0,
      y=0,
      velx=0,
      vely=0,
      r=0,
      alive=false,
      mass=0
    })
  end
end

function draw_sparks()
  for i=1,#sparks do
    if sparks[i].alive then
      circfill(
        sparks[i].x,
        sparks[i].y,
        sparks[i].r,
        5+rnd(5)
      )
    end
  end
end
-->8
--actions
function move_player()
  p.dy+=g --add gravity
  
  thrust()
  
  p.x+=p.dx --actually move
  p.y+=p.dy --the player
  
  stay_on_screen()
end

function thrust()
  --add thrust to movement
  if (btn(0)) p.dx-=p.thrust
  if (btn(1)) p.dx+=p.thrust
  if (btn(2)) p.dy-=p.thrust
  
  --thrust sound
  if (btn(0) or btn(1) or btn(2)) sfx(0)
end
-->8
--util
function check_land()
  l_x=flr(p.x) --left side of ship
  r_x=flr(p.x+7) --right side of ship
  b_y=flr(p.y+7) --bottom of ship
  
  over_pad=l_x>=pad.x and r_x<=pad.x+pad.width
  on_pad=b_y>=pad.y-1
  slow=p.dy<1
  
  if (over_pad and on_pad and slow) then
    next_level()
  elseif ((over_pad and on_pad) or (fuel == 0))then
    explode(p.x,p.y,5,5+rnd(5))
    end_game()
  else
    for i=l_x,r_x do
      if (gnd[i]<=b_y) then
        explode(p.x,p.y,5,5+rnd(5))
        end_game()
      end
    end
  end
end

function end_game()
  game_over=true
  score += fuel
  sfx(2)
end

function next_level()
  lvlup=true
  score += fuel
  sfx(1)
end  

function stay_on_screen()
  if (p.x<0) then --left side
    p.x=0
    p.dx=0
  end
  
  if (p.x>119) then --right side
    p.x=119
    p.dx=0
  end
  
  if (p.y<0) then --top side
    p.y=0
    p.dy=0
  end
end

--custom random function
function rndb(low,high)
  return flr(rnd(high-low+1)+low)
end

--center text
function hcenter(s)
  return 64-#s*2
end

function toggle_music()
  if (music_playing) then
    music(-1)
    music_playing=false
  else
    music(0)
    music_playing=true
  end
end

function explode(x,y,r,particles)
  local selected=0
  for i=1,#sparks do
    if not sparks[i].alive then
      sparks[i].x = x
      sparks[i].y = y
      sparks[i].vely = -1 + rnd(2)
      sparks[i].velx = -1 + rnd(2)
      sparks[i].mass = 0.5 + rnd(2)
      sparks[i].r = 0.5 + rnd(r)
      sparks[i].alive = true
      
      selected += 1
      if selected == particles then
        break
      end
    end
  end
end

function calculate_fuel()
  fuel -= 1
end
__gfx__
0000000000055000761dddddddddd766000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000550007666666666666666000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000005c75000076666666666600000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000055cc5500000000000000000000b60000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000555cc555000000000000000000bb60000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000000005555555500000000000000000bbb60000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000055555500000000000000000000060000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000060660600000000000000000000060000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
00000000099000000000009999000990000009900999999999009999999099999990000000000000000000000000000000000000000000000000000000000000
0000000009900000000000999900099990000990099dddddd99099ddddd09900dd99000000000000000000000000000000000000000000000000000000000000
0000000009900000000099dddd990999900009900990000009909900000099000099000000000000000000000000000000000000000000000000000000000000
00000000099000000000990000990999999009900990000009909900000099000099000000000000000000000000000000000000000000000000000000000000
0000000009900000000099000099099dd99009900990000009909900000099000099000000000000000000000000000000000000000000000000000000000000
00000000099000000000990000990990099999900990000009909900000099000099000000000000000000000000000000000000000000000000000000000000
000000000990000000009900009909900dd999900990000009909900000099000099000000000000000000000000000000000000000000000000000000000000
000000000990000000009999999909900009999009900000099099999900999999dd000000000000000000000000000000000000000000000000000000000000
0000000009900000000099dddd990990000dd99009900000099099dddd0099dddd00000000000000000000000000000000000000000000000000000000000000
00000000099000000000990000990990000009900990000009909900000099000099000000000000000000000000000000000000000000000000000000000000
00000000099000000000990000990990000009900990000009909900000099000099000000000000000000000000000000000000000000000000000000000000
00000000099000000000990000990990000009900990000009909900000099000099000000000000000000000000000000000000000000000000000000000000
00000000099000000000990000990990000009900990000009909900000099000099000000000000000000000000000000000000000000000000000000000000
00000000099999999990990000990990000009900999999999d09999999099000099000000000000000000000000000000000000000000000000000000000000
000000000dddddddddd0dd0000dd0dd000000dd00ddddddddd00ddddddd0dd0000dd000000000000000000000000000000000000000000000000000000000000
__sfx__
000600000363000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000c00001c0701c030190701903016070160301b0701b03000600160701d0701d0300000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
000400003d67037660316502c64027640216301d6301863015620116200e6200b6200961006610046100261001610016100000000000000000000000000000000000000000000000000000000000000000000000
011000002205022050220502205022050220502205023050240502505026050270502a0502c0502f0503205034050350503505035050350403503035020000000000000000000000000000000000000000000000
011000002e5002c5002b5002850026500235001f5001c500000000000000000000000000000000000002e5522c5522b5522855226552235521f5521c552185520000200000000000000000000000000000000000
__music__
00 03044344

